<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details des artistes</title>
</head>
<body>
	<h1 align="center">L'artiste est enregistr� avec succ�s ! </h1>
	<form action="/WEB-INF/views/artisteDetails.jsp">
	<div align="center">
 		<a href="Artiste?param=value">Cr�er un autre artiste</a>
 	</div>
	 <div align="center">
 		<a href="Album?param=value">Cr�er un album</a>
 	 </div>
 	 <div align="center">
 		<a href="MenuServlet?param=value">Retour au Menu</a>
 	</div>
	</form>
	 <ul>
        <c:forEach var="artiste" items="${ artistes }">
            <li><c:out value="ID : ${ artiste.artisteID } | Nom de l'artiste : ${ artiste.nom } | Nationalit� de l'artiste : ${ artiste.nationalite }" /></li>
        </c:forEach>
    </ul>
</body>
</html>