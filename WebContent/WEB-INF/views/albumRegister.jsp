<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Page Register</title>
 	<a href="MenuServlet?param=value">Retour au Menu</a>
</head>
<body>
 <div align="center">
  <h1>Enregister un album</h1>
  <form action="<%= request.getContextPath() %>/Album" method="post">
   <table style="with: 80%">
    <tr>
     <td>Titre/Description</td>
     <td><input type="text" name="description" /></td>
    </tr>
    <tr>
    <td>Artiste</td>
    <td>
    <select name="artiste">
        <c:forEach var="artiste" items="${ artistes }">
            <option value= ${artiste.nom }> ${artiste.nom}</option>
        </c:forEach>	
	</select>
	</td>
    <tr>
     <td>Ann�e de sortie</td>
     <td><input type="text" name="dateDeSortie" /></td>
    </tr>
     <tr>
     <td>Prix d'achat</td>
     <td><input type="text" name="prixAchat" /></td>
    </tr>
     <tr>
     <td>Pistes</td>
     <td><input type="text" name="pistes" /></td>
    </tr>
   </table>
   <input type="submit" value="Enregistrer" />
  </form>
 </div>
</body>
</html>
