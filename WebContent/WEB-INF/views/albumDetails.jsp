<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Album enregistr�</title>
</head>
<body>
	<h1 align="center">L'album est enregistr� avec succ�s ! </h1>
	<form action="/WEB-INF/views/artisteDetails.jsp">
	 <div align="center">
 		<a href="Album?param=value">Cr�er un autre album</a>
 	 </div>
 	 <div align="center">
 		<a href="Artiste?param=value">Cr�er un artiste</a>
 	</div>
 	 <div align="center">
 		<a href="MenuServlet?param=value">Retour au Menu</a>
 	</div>
	</form>
	<ul>
        <c:forEach var="album" items="${ albums }">
            <li><c:out value="ID : ${ album.albumID } | Nom de l'album : ${ album.description } | Ann�e de sortie de l'album : ${ album.dateDeSortie } | Prix d'achat de l'album : ${ album.prixAchat } | les pistes de l'album : ${ album.pistes }" /></li>
        </c:forEach>
    </ul>
	
</body>
</html>