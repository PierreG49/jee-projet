Pour la BDD en local :

Nom de database : "java-ee"

CREATE TABLE Artiste (
    artisteID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(30),
    nationalite VARCHAR(30)
);

CREATE TABLE Album (
    albumID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    description VARCHAR(100),
    dateDeSortie INT(4),
    prixAchat DECIMAL(5,2),
    pistes VARCHAR(100)
);
