package projet.java.beans;

public class Album {
	private int albumID;
	private String description;
	private int dateDeSortie;
	private float prixAchat;
	private String pistes;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDateDeSortie() {
		return dateDeSortie;
	}
	public void setDateDeSortie(int dateDeSortie) {
		this.dateDeSortie = dateDeSortie;
	}
	public float getPrixAchat() {
		return prixAchat;
	}
	public void setPrixAchat(float prixAchat) {
		this.prixAchat = prixAchat;
	}
	public String getPistes() {
		return pistes;
	}
	public void setPistes(String pistes) {
		this.pistes = pistes;
	}
	public int getAlbumID() {
		return albumID;
	}
	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}
	

	
}
