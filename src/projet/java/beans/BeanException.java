package projet.java.beans;

public class BeanException extends Exception {
    public BeanException(String message) {
        super(message);
    }
}