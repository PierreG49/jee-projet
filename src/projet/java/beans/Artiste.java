package projet.java.beans;

public class Artiste {
	private int artisteID;
	private String nom;
	private String nationalite;
	
	public String getNom() {
		return nom;
	}
	
//	public void setNom(String nom) {
//		this.nom = nom;
//	}
	
	public void setNom(String nom) throws BeanException {
        if (nom.length() > 50) {
            throw new BeanException("Le nom doit contenire moins de 50 caractéres");
        }
        else {
            this.nom = nom; 
        }
    }
	
	public String getNationalite() {
		return nationalite;
	}
	
//	public void setNationalite(String nationalite) {
//		this.nationalite = nationalite;
//	}
	
	public void setNationalite(String nationalite) throws BeanException {
        if (nationalite.length() > 100) {
            throw new BeanException("La nationalité doit contenire moins de 30 caractéres");
        }
        else {
            this.nationalite = nationalite; 
        }
    }
	
	public int getArtisteID() {
		return artisteID;
	}

	public void setArtisteID(int artisteID) {
		this.artisteID = artisteID;
	}
	
	
}
