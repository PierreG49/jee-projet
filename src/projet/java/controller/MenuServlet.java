package projet.java.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet.java.beans.BeanException;
import projet.java.dao.DaoFactory;
import projet.java.dao.AlbumDao;
import projet.java.dao.DaoException;

/**
 * Servlet implementation class MenuServlet
 */
@WebServlet("/MenuServlet")
public class MenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AlbumDao albumDao;
	
    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.albumDao = daoFactory.getAlbumDao();
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
            request.setAttribute("albums", albumDao.listerDESC());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        } catch (BeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
            request.setAttribute("albumTotal", albumDao.total());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        } catch (BeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/menuNav.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/menuNav.jsp").forward(request, response);
	}

}
