package projet.java.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet.java.dao.ArtisteDao;
import projet.java.dao.DaoFactory;

/**
 * Servlet implementation class ArtisteListeServlet
 */
@WebServlet("/ArtisteListeServlet")
public class ArtisteListeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ArtisteDao artisteDao;
	
    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.artisteDao = daoFactory.getArtisteDao();
    }
    
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArtisteListeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
        try {
            request.setAttribute("artistes", artisteDao.lister());
        }
        catch (Exception e) {
        	e.printStackTrace();
            request.setAttribute("erreur", e.getMessage());
        }
        
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/artisteListe.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
