package projet.java.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet.java.beans.Artiste;
import projet.java.beans.BeanException;
import projet.java.dao.DaoFactory;
import projet.java.dao.AlbumDao;
import projet.java.dao.ArtisteDao;
import projet.java.dao.DaoException;

/**
 * Servlet implementation class Artiste
 */
@WebServlet("/Artiste")
public class ArtisteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ArtisteDao artisteDao;
	private AlbumDao albumDao;

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.artisteDao = daoFactory.getArtisteDao();
        this.albumDao = daoFactory.getAlbumDao();
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArtisteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
            request.setAttribute("artistes", artisteDao.lister());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        } catch (BeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/artisteRegister.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
        Artiste artiste = new Artiste();
        try {
			artiste.setNom(request.getParameter("nom"));
		} catch (BeanException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			artiste.setNationalite(request.getParameter("nationalite"));
		} catch (BeanException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
            
        try {
            artisteDao.ajouter(artiste);
            request.setAttribute("artistes", artisteDao.lister());
        }
        catch (Exception e) {
        	e.printStackTrace();
            request.setAttribute("erreur", e.getMessage());
        }
		try {
            request.setAttribute("albums", albumDao.listerDESC());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        } catch (BeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
            request.setAttribute("albumTotal", albumDao.total());
        }
        catch (DaoException e) {
            request.setAttribute("erreur", e.getMessage());
        } catch (BeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/menuNav.jsp").forward(request, response);
	}

}