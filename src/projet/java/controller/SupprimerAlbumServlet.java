package projet.java.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projet.java.dao.AlbumDao;
import projet.java.dao.DaoFactory;

/**
 * Servlet implementation class SupprimerAlbumServlet
 */
@WebServlet("/SupprimerAlbumServlet")
public class SupprimerAlbumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private AlbumDao albumDao;
	

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.albumDao = daoFactory.getAlbumDao();
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimerAlbumServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            albumDao.supprimer(Integer.parseInt(request.getParameter("param")));
        }
        catch (Exception e) {
        	e.printStackTrace();
            request.setAttribute("erreur", e.getMessage());
        }
        
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/supprimer.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
