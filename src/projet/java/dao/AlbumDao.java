package projet.java.dao;

import java.util.List;

import projet.java.beans.Album;
import projet.java.beans.BeanException;

public interface AlbumDao {
	void ajouter( Album album ) throws DaoException;
    List<Album> lister() throws DaoException, BeanException;
	int total() throws DaoException, BeanException;
	List<Album> listerDESC() throws DaoException, BeanException;
	void supprimer(int ID) throws DaoException;
}