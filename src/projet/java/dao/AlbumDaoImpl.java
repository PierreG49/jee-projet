package projet.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import projet.java.beans.Album;
import projet.java.beans.BeanException;

public class AlbumDaoImpl implements AlbumDao {
	private DaoFactory daoFactory;

    AlbumDaoImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void ajouter(Album album) throws DaoException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
        	// connexion � la BDD
            connexion = daoFactory.getConnection();
            // requ�te pr�par�e
            preparedStatement = connexion.prepareStatement("INSERT INTO album(albumID, description, dateDeSortie, prixAchat, pistes) VALUES(?, ?, ?, ?, ?);");
            preparedStatement.setInt(1, 0);
            preparedStatement.setString(2, album.getDescription());
            preparedStatement.setInt(3, album.getDateDeSortie());
            preparedStatement.setFloat(4, album.getPrixAchat());
            preparedStatement.setString(5, album.getPistes());
            
            // ex�cution de la requ�te
            preparedStatement.executeUpdate();
            connexion.commit();
        } catch (SQLException e) {
        	e.printStackTrace();
            try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {
            }
            throw new DaoException("Impossible de communiquer avec la base de donn�es (1)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (2)");
            }
        }

    }

    @Override
    public List<Album> lister() throws DaoException, BeanException {
        List<Album> albums = new ArrayList<Album>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT albumID, description, dateDeSortie, prixAchat, pistes FROM album;");

            while (resultat.next()) {

            	int albumID = resultat.getInt("albumID");
                String description = resultat.getString("description");
                int dateDeSortie = resultat.getInt("dateDeSortie");
                float prixAchat = resultat.getFloat("prixAchat");
                String pistes = resultat.getString("pistes");

                Album album = new Album();
                album.setAlbumID(albumID);
                album.setDescription(description);
                album.setDateDeSortie(dateDeSortie);
                album.setPrixAchat(prixAchat);
                album.setPistes(pistes);

                albums.add(album);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DaoException("Impossible de communiquer avec la base de donn�es (3)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (4)");
            }
        }
        return albums;
    }
  
	@Override
    public List<Album> listerDESC() throws DaoException, BeanException {
        List<Album> albums = new ArrayList<Album>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT albumID, description, dateDeSortie, prixAchat, pistes FROM album ORDER BY albumID DESC;");

            while (resultat.next()) {

            	int albumID = resultat.getInt("albumID");
                String description = resultat.getString("description");
                int dateDeSortie = resultat.getInt("dateDeSortie");
                float prixAchat = resultat.getFloat("prixAchat");
                String pistes = resultat.getString("pistes");

                Album album = new Album();
                album.setAlbumID(albumID);
                album.setDescription(description);
                album.setDateDeSortie(dateDeSortie);
                album.setPrixAchat(prixAchat);
                album.setPistes(pistes);

                albums.add(album);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DaoException("Impossible de communiquer avec la base de donn�es (3)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (4)");
            }
        }
        return albums;
    }
    
    @Override
    public int total() throws DaoException, BeanException {
        int albums = 0;
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT COUNT(albumID) as nbTot FROM album;");

            if (resultat.next()) {
            	int nbAlbums = resultat.getInt("nbTot");
            

                albums = nbAlbums;
            }
        }
            
         catch (SQLException e) {
        	e.printStackTrace();
            throw new DaoException("Impossible de communiquer avec la base de donn�es (3)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (4)");
            }
        }
        return albums;
    }
    
	@Override
    public void supprimer(int ID) throws DaoException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
        	// connexion � la BDD
            connexion = daoFactory.getConnection();
            // requ�te pr�par�e
            preparedStatement = connexion.prepareStatement("DELETE FROM `album` WHERE albumID = "
            		+ ID
            		+ ";");
            
            // ex�cution de la requ�te
            preparedStatement.executeUpdate();
            connexion.commit();
        } catch (SQLException e) {
        	e.printStackTrace();
            try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {
            }
            throw new DaoException("Impossible de communiquer avec la base de donn�es (1)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (2)");
            }
        }

    }
}
