package projet.java.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoFactory {
    private String url;
    private String username;
    private String password;

    DaoFactory(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public static DaoFactory getInstance() {
        try {
        	Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }

        DaoFactory instance = new DaoFactory(
        		"jdbc:mysql://localhost:3306/java-ee?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connexion = DriverManager.getConnection(url, username, password);
        connexion.setAutoCommit(false);
        return connexion;
    }

    // Récupération du Dao de la classe Artiste
    public ArtisteDao getArtisteDao() {
        return new ArtisteDaoImpl(this);
    }
    
    // Récupération du Dao de la classe Album
    public AlbumDao getAlbumDao() {
        return new AlbumDaoImpl(this);
    }
}
