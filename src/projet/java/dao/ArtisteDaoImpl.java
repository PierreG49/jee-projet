package projet.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import projet.java.beans.Artiste;
import projet.java.beans.BeanException;

public class ArtisteDaoImpl implements ArtisteDao {
	private DaoFactory daoFactory;

    ArtisteDaoImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

	@Override
    public void ajouter(Artiste artiste) throws DaoException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
        	// connexion � la BDD
            connexion = daoFactory.getConnection();
            // requ�te pr�par�e
            preparedStatement = connexion.prepareStatement("INSERT INTO artiste(artisteID, nom, nationalite) VALUES(?, ?, ?);");
            preparedStatement.setInt(1, 0);
            preparedStatement.setString(2, artiste.getNom());
            preparedStatement.setString(3, artiste.getNationalite());

            
            // ex�cution de la requ�te
            preparedStatement.executeUpdate();
            connexion.commit();
        } catch (SQLException e) {
        	e.printStackTrace();
            try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {
            }
            throw new DaoException("Impossible de communiquer avec la base de donn�es (1)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (2)");
            }
        }

    }

    @Override
    public List<Artiste> lister() throws DaoException, BeanException {
        List<Artiste> artistes = new ArrayList<Artiste>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT artisteID, nom, nationalite FROM artiste;");

            while (resultat.next()) {
            	int artisteID = resultat.getInt("artisteID");
                String nom = resultat.getString("nom");
                String nationalite = resultat.getString("nationalite");

                Artiste artiste = new Artiste();
                artiste.setArtisteID(artisteID);
                artiste.setNom(nom);
                artiste.setNationalite(nationalite);

                artistes.add(artiste);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new DaoException("Impossible de communiquer avec la base de donn�es (3)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (4)");
            }
        }
        return artistes;
    }
    
	@Override
    public void supprimer(int ID) throws DaoException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
        	// connexion � la BDD
            connexion = daoFactory.getConnection();
            // requ�te pr�par�e
            preparedStatement = connexion.prepareStatement("DELETE FROM `artiste` WHERE artisteID = "
            		+ ID
            		+ ";");
            
            // ex�cution de la requ�te
            preparedStatement.executeUpdate();
            connexion.commit();
        } catch (SQLException e) {
        	e.printStackTrace();
            try {
                if (connexion != null) {
                    connexion.rollback();
                }
            } catch (SQLException e2) {
            }
            throw new DaoException("Impossible de communiquer avec la base de donn�es (1)");
        }
        finally {
            try {
                if (connexion != null) {
                    connexion.close();  
                }
            } catch (SQLException e) {
                throw new DaoException("Impossible de communiquer avec la base de donn�es (2)");
            }
        }

    }
}
