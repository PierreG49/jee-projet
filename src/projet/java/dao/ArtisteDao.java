package projet.java.dao;

import java.util.List;

import projet.java.beans.BeanException;
import projet.java.beans.Artiste;


public interface ArtisteDao {
	void ajouter( Artiste artiste ) throws DaoException;
    List<Artiste> lister() throws DaoException, BeanException;
	void supprimer(int ID) throws DaoException;
}